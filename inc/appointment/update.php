<?php 
	session_start();

	if (isset($_POST['update'])) {

		include_once '../db.php';

		$id = $_POST['id'];

		$patientName = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['patient_name']));
		$birthday = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['birthday']));
		$patientDoc = $_POST['patient_doc'];
		$patientSex = $_POST['patient_sex'];
		$patientCont = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['patient_contact']));
		$schedule = $_POST['schedule'];
		$diseaseDesc = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['disease_desc']));

		if (empty($patientName) || empty($birthday) || empty($patientDoc) || empty($patientCont) || empty($diseaseDesc)) {
			$_SESSION['error'] = 'You must fill Name, Birthday, Contact, Doctor and Description.';
			header('Location: ../../appointment_edit.php?id='.$id);
			exit();
		}else{
			$sqlQuery = "UPDATE appointments SET patient_name = '$patientName', birthday = '$birthday', patient_doc = '$patientDoc', patient_sex = '$patientSex', patient_contact = '$patientCont', schedule = '$schedule', disease_desc = '$diseaseDesc' WHERE id = '$id'";
			$result = mysqli_query($conn, $sqlQuery);
			if ($result == true) {
				$_SESSION['success'] = 'Appointments\'s Data has been updated successfully';
				header('Location: ../../appointment.php?check=success');
				exit();
			}else{
				$_SESSION['error'] = 'Something is happend wrong! Data has not updated';
				header('Location: ../../appointment_edit.php?id='.$id);
				exit();
			}
		}

	}