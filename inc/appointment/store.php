<?php 
	session_start();

	if (isset($_POST['submit'])) {

		include_once '../db.php';

		$patientName = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['patient_name']));
		$birthday = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['birthday']));
		$patientDoc = $_POST['patient_doc'];
		$patientSex = $_POST['patient_sex'];
		$patientCont = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['patient_contact']));
		$schedule = $_POST['schedule'];

		$doctor_fee = floatval(trim($_POST['doctor_fee']));
		$test_fee = floatval(trim($_POST['test_fee']));
		$others_fee = 0;
		$others_fee = $others_fee + floatval(trim($_POST['other_fee']));
		$discount = 0;
		$discount = $discount + floatval(trim($_POST['discount']));
		$total = $doctor_fee + $test_fee + $others_fee;
		$discount_amt = ($total * $discount / 100);
		$total_amt = $total - $discount_amt;

		$diseaseDesc = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['disease_desc']));

		
		if (empty($patientName) || empty($birthday) || empty($patientDoc) || empty($patientCont) || empty($diseaseDesc) || empty($doctor_fee) || empty($test_fee)) {
			$_SESSION['error'] = 'You must fill Name, Birthday, Contact, Doctor, Doctor\'s Fee, Test Fee  and Description.';
			header('Location: ../../appointment_add.php?check=error');
			exit();
		}else{
			$sqlQuery = "INSERT INTO appointments (patient_name, birthday, patient_doc, patient_sex, patient_contact, schedule, doctor_fee, test_fee, others_fee, discount_amt, total_amt, disease_desc) VALUES ('$patientName', '$birthday', '$patientDoc', '$patientSex', '$patientCont', '$schedule', '$doctor_fee', '$test_fee', '$others_fee', '$discount_amt', '$total_amt', '$diseaseDesc')";
			$result = mysqli_query($conn, $sqlQuery);
			if ($result == true) {
				$_SESSION['success'] = 'Appointment has been made successfully';
				header('Location: ../../appointment.php?check=success');
				exit();
			}else{
				$_SESSION['error'] = 'Something is happend wrong! Data has not inserted';
				header('Location: ../../appointment_add.php?check=error');
				exit();
			}
		}

	}