<?php
	session_start();

	if (isset($_GET['id'])) {
		include_once '../db.php';

		$id = $_GET['id'];

		$sqlQuery = "DELETE FROM appointments WHERE id = '$id'";
		$result = mysqli_query($conn, $sqlQuery);

		if ($result) {
			$_SESSION['success'] = 'Appointment data has been deleted successfully';
			header('Location: ../../appointment.php?check=success');
			exit();
		}
	}