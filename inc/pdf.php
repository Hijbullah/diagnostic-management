<?php
// Require composer autoload
require_once __DIR__ . '/vendor/autoload.php';
// Create an instance of the class:
$mpdf = new \Mpdf\Mpdf();

require_once 'db.php';

if(isset($_GET['id'])){
    $id = $_GET['id'];

    $sqlQuery = "SELECT * FROM appointments Where id = ". $id;
    $appointments = mysqli_query($conn, $sqlQuery);
}else{
    $sqlQuery = "SELECT * FROM appointments";
    $appointments = mysqli_query($conn, $sqlQuery);
}


$trs="";
$sl=0;

while ($appointment = mysqli_fetch_array($appointments)){
$patientName = $appointment['patient_name'];
$patientCont = $appointment['patient_contact'];
$patientDoc =  $appointment['patient_doc'];
$docFee = $appointment['doctor_fee'];
$testFee = $appointment['test_fee'];
$otherFee = $appointment['others_fee'];
$discAmt = $appointment['discount_amt'];
$totalAmt = $appointment['total_amt'];
$sl++;
$trs .= "<tr>";
$trs .= "<td>$sl</td>";
$trs .= "<td>$patientName</td>";
$trs .= "<td>$patientCont</td>";
$trs .= "<td>$patientDoc</td>";
$trs .= "<td>$docFee</td>";
$trs .= "<td>$testFee</td>";
$trs .= "<td>$otherFee</td>";
$trs .= "<td>$discAmt</td>";
$trs .= "<td>$totalAmt</td>";
$trs .= "</tr>";
}

$html = <<<DM
<h2 align="center">Disgonostic Management</h2>
<p align="center">Billing Info</p>
<table border="1" style= "border-collapse: collapse">
    <thead>
        <tr>
            <th rowspan="2">SL.</th>
            <th rowspan="2">Patient Name</th>
            <th rowspan="2">Patient Contact</th>
            <th rowspan="2">Appointed To</th>
            <th colspan="5" class="text-center">Payment Details</th>
        </tr>
        <tr>
            <th>Doctor Fee (Tk.)</th>
            <th>Test Fee (TK.)</th>
            <th>Others fee (Tk.)</th>
            <th>Discount (Tk.)</th>
            <th>Total (Tk.)</th>
        </tr>
    </thead>
    <tbody>
        $trs
    </tbody>
</table>
DM;

// Write some HTML code:
$mpdf->WriteHTML($html);
// Output a PDF file directly to the browser
$mpdf->Output();