<?php
session_start();

######## PLEASE PROVIDE Your Gmail Info. -  (ALLOW LESS SECURE APP ON GMAIL SETTING ) ########
$GmailAddress = 'diagnostic.project.m3@gmail.com';
$GmailPassword = 'projectm3';
##############################################################################################
include_once('vendor/autoload.php');
require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';

require_once 'db.php';

if(isset($_GET['id'])){
    $id = $_GET['id'];

    $sqlQuery = "SELECT * FROM appointments WHERE id= ".$id;
    $appointments = mysqli_query($conn, $sqlQuery); 
}else{
    $sqlQuery = "SELECT * FROM appointments";
    $appointments = mysqli_query($conn, $sqlQuery);
}


$trs="";
$sl=0;

while ($appointment = mysqli_fetch_array($appointments)){
$patientName = $appointment['patient_name'];
$patientCont = $appointment['patient_contact'];
$patientDoc =  $appointment['patient_doc'];
$docFee = $appointment['doctor_fee'];
$testFee = $appointment['test_fee'];
$otherFee = $appointment['others_fee'];
$discAmt = $appointment['discount_amt'];
$totalAmt = $appointment['total_amt'];
$sl++;
$trs .= "<tr>";
$trs .= "<td>$sl</td>";
$trs .= "<td>$patientName</td>";
$trs .= "<td>$patientCont</td>";
$trs .= "<td>$patientDoc</td>";
$trs .= "<td>$docFee</td>";
$trs .= "<td>$testFee</td>";
$trs .= "<td>$otherFee</td>";
$trs .= "<td>$discAmt</td>";
$trs .= "<td>$totalAmt</td>";
$trs .= "</tr>";
}

$html = <<<DM
<h2 align="center">Disgonostic Management</h2>
<p align="center">Billing Info</p>
<table border="1" style= "border-collapse: collapse">
    <thead>
        <tr>
            <th rowspan="2">SL.</th>
            <th rowspan="2">Patient Name</th>
            <th rowspan="2">Patient Contact</th>
            <th rowspan="2">Appointed To</th>
            <th colspan="5" class="text-center">Payment Details</th>
        </tr>
        <tr>
            <th>Doctor Fee (Tk.)</th>
            <th>Test Fee (TK.)</th>
            <th>Others fee (Tk.)</th>
            <th>Discount (Tk.)</th>
            <th>Total (Tk.)</th>
        </tr>
    </thead>
    <tbody>
        $trs
    </tbody>
</table>
DM;


?>

<!DOCTYPE html>
<head>
    <title>Email | Billing Info</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

</head>
<body>
    <div class="container">
        <div class="col-md-8 offset-md-2">
            <h2 class="mt-5">Email | Billing Info </h2>
            <form  role="form" method="post" action="email.php?billing-info=1">
                <div class="form-group">
                    <label for="Name">Name:</label>
                    <input type="text"  name="name"  class="form-control form-control-sm" id="name" placeholder="Enter name of the recipient" required>
                </div>
                <div class="form-group">
                    <label for="Email">Email Address:</label>
                    <input type="email"  name="email"  class="form-control form-control-sm" id="Email" placeholder="Enter recipient email address here... " required>
                </div>
                <div class="form-group">
                    <label for="Subject">Subject:</label>
                    <input type="text"  name="subject"  class="form-control form-control-sm" id="subject" value="Billing Info">
                </div>
                <div class="form-group">
                    <label for="body">Body:</label>
                    <textarea rows="8" cols="160" name="body"><?php printf($html); ?></textarea>
                </div>
                <input class="btn btn-md btn-success mb-5" type="submit" value="Send Email">
            </form>
        </div>
    </div> 

    <?php
        if(isset($_REQUEST['email'])&&isset($_REQUEST['subject'])) {
            date_default_timezone_set('Etc/UTC');
            //Create a new PHPMailer instance
            $mail = new \PHPMailer\PHPMailer\PHPMailer();
            //Tell PHPMailer to use SMTP
            $mail->isSMTP();
            //Enable SMTP debugging
            // 0 = off (for production use)
            // 1 = client messages
            // 2 = client and server messages
            $mail->SMTPDebug = 0;
            //Ask for HTML-friendly debug output
            $mail->Debugoutput = 'html';
            //Set the hostname of the mail server
            $mail->Host = 'smtp.gmail.com';
            // use
            // $mail->Host = gethostbyname('smtp.gmail.com');
            // if your network does not support SMTP over IPv6
            //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
            $mail->Port = 587; //587
            //Set the encryption system to use - ssl (deprecated) or tls
            $mail->SMTPSecure = 'tls'; //tls
            //Whether to use SMTP authentication
            $mail->SMTPAuth = true;
            //Username to use for SMTP authentication - use full email address for gmail
            $mail->Username = $GmailAddress;
            //Password to use for SMTP authentication
            $mail->Password = $GmailPassword;
            //Set who the message is to be sent from
            $mail->setFrom($GmailAddress, 'Diagonostic Management');
            //Set an alternative reply-to address
            $mail->addReplyTo($GmailAddress, 'Diagonostic Management');
            //Set who the message is to be sent to
            //echo $_REQUEST['email']; die();
            $mail->addAddress($_REQUEST['email'], $_REQUEST['name']);
            //Set the subject line
            $mail->Subject = $_REQUEST['subject'];
            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            //$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
            //Replace the plain text body with one created manually
            $mail->AltBody = 'This is a plain-text message body';
            $mail->Body = $_REQUEST['body'];
            if (!$mail->send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
            }
            else{
                $_SESSION['success'] = "Email has been sent sucessfully";
                header('Location: ../accounts.php');
            }
        }
    ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script>tinymce.init({
    selector: 'textarea',  // change this value according to your HTML
    menu: {
    table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
    tools: {title: 'Tools', items: 'spellchecker code'}
    }
    });
    </script>
</body>
</html>