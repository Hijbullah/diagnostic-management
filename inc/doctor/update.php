<?php 
	session_start();

	if (isset($_POST['submit'])) {

		include_once '../db.php';
		$id = $_POST['id'];
		$doctorName = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['doctor_name']));
		$doctorDesig = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['doctor_designation']));
		$doctorCont = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['doctor_contact']));

		if( $_FILES["doctor_img"] ['name']){
	       $img =   time().$_FILES["doctor_img"]["name"];
	       $source = $_FILES["doctor_img"]["tmp_name"];
	       $destination = "../../img/Uploads/".$img;
	       move_uploaded_file($source, $destination);
     
   		}

		if (empty($doctorName) || empty($doctorDesig) || empty($doctorCont)) {
			$_SESSION['error'] = 'You must fill Doctor Name, Designation and Contact';
			header('Location: ../../doctor_edit.php?id='.$id);
			exit();
		}else{
			$sqlQuery = "UPDATE doctors SET name = '$doctorName', designation = '$doctorDesig', contact = '$doctorCont', image = '$img' WHERE id = '$id'";
			$result = mysqli_query($conn, $sqlQuery);
			if ($result == true) {
				$_SESSION['success'] = 'Doctor\'s Data has been updated successfully';
				header('Location: ../../doctor.php?check=success');
				exit();
			}else{
				$_SESSION['error'] = 'Something is happend wrong! Data has not inserted';
				header('Location: ../../doctor_edit.php?id='.$id);
				exit();
			}
		}

	}