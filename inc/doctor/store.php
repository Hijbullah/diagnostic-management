<?php 
	session_start();

	if (isset($_POST['submit'])) {

		include_once '../db.php';

		$doctorName = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['doctor_name']));
		$doctorDesig = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['doctor_designation']));
		$doctorCont = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['doctor_contact']));

		if( $_FILES["doctor_img"] ['name']){
	       $img =   time().$_FILES["doctor_img"]["name"];
	       $source = $_FILES["doctor_img"]["tmp_name"];
	       $destination = "../../img/Uploads/".$img;
	       move_uploaded_file($source, $destination);
     
   		}
   		

		if (empty($doctorName) || empty($doctorDesig) || empty($doctorCont)) {
			$_SESSION['error'] = 'You must fill Doctor Name, Designation and Contact';
			header('Locaion: ../../doctor_add.php?check=error');
			exit();
		}else{
			$sqlQuery = "INSERT INTO doctors (name, designation, contact, image) VALUES ('$doctorName', '$doctorDesig', '$doctorCont', '$img')";
			$result = mysqli_query($conn, $sqlQuery);
			if ($result == true) {
				$_SESSION['success'] = 'New Doctor add successfully';
				header('Location: ../../doctor.php?check=success');
				exit();
			}else{
				$_SESSION['error'] = 'Something is happend wrong! Data has not inserted';
				header('Location: ../../doctor_add.php?check=error');
				exit();
			}
		}

	}