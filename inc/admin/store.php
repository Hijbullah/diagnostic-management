<?php 
	session_start();

	if (isset($_POST['submit'])) {

		include_once '../db.php';

		$adminName = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['admin_name']));
		$adminEmail = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['admin_email']));
		$adminPass = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['admin_pass']));

		if (empty($adminName) || empty($adminEmail) || empty($adminPass)) {
			$_SESSION['error'] = 'You must fill Admin Name, Email and Password';
			header('Location: ../../admin_add.php');
			exit();
		}else{
			$adminPass = md5($adminPass);
			
			$sqlQuery = "INSERT INTO admins (name, email, password) VALUES ('$adminName', '$adminEmail', '$adminPass')";
			$result = mysqli_query($conn, $sqlQuery);
			if ($result == true) {
				$_SESSION['success'] = 'New Admin add successfully';
				header('Location: ../../admin.php?check=success');
				exit();
			}else{
				$_SESSION['error'] = 'Something is happend wrong! Data has not inserted';
				header('Location: ../../admin_add.php?check=error');
				exit();
			}
		}

	}